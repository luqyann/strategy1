// Fill out your copyright notice in the Description page of Project Settings.

#include "StrategyFunctionLibrary.h"
#include "SceneView.h"
#include "Components/SceneCaptureComponent2D.h"
#include "Widgets/SViewport.h"
#include "SceneManagement.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/LocalPlayer.h"



bool UStrategyFunctionLibrary::EqualPS(const TArray<APlayerState*>& a, const TArray<APlayerState*>& b) {
	return a == b;
}

FMatrix UStrategyFunctionLibrary::GetMatrixFromTransform(const FTransform & t)
{
	
	return t.ToMatrixWithScale();
}

FMatrix UStrategyFunctionLibrary::GetPerspectiveMatrix(float HalfFOVX, float HalfFOVY, float MultFOVX, float MultFOVY, float MinZ, float MaxZ)
{
	
	
	return FPerspectiveMatrix::FPerspectiveMatrix( HalfFOVX,  HalfFOVY,  MultFOVX,  MultFOVY,  MinZ,  MaxZ);
}

FMatrix UStrategyFunctionLibrary::MultiplyMatrix(const FMatrix & a, const FMatrix & b)
{
	
	return a * b;
}
//extern void BuildProjectionMatrix(FIntPoint RenderTargetSize, ECameraProjectionMode::Type ProjectionType, float FOV, float InOrthoWidth, FMatrix& ProjectionMatrix);
namespace {
	
	void BuildProjectionMatrix(FIntPoint RenderTargetSize, ECameraProjectionMode::Type ProjectionType, float FOV, float InOrthoWidth, FMatrix& ProjectionMatrix)
	{
		float const XAxisMultiplier = 1.0f;
		float const YAxisMultiplier = RenderTargetSize.X / (float)RenderTargetSize.Y;

		if (ProjectionType == ECameraProjectionMode::Orthographic)
		{
			check((int32)ERHIZBuffer::IsInverted);
			const float OrthoWidth = InOrthoWidth / 2.0f;
			const float OrthoHeight = InOrthoWidth / 2.0f * XAxisMultiplier / YAxisMultiplier;

			const float NearPlane = 0;
			const float FarPlane = WORLD_MAX / 8.0f;

			const float ZScale = 1.0f / (FarPlane - NearPlane);
			const float ZOffset = -NearPlane;

			ProjectionMatrix = FReversedZOrthoMatrix(
				OrthoWidth,
				OrthoHeight,
				ZScale,
				ZOffset
			);
		}
		else
		{
			if ((int32)ERHIZBuffer::IsInverted)
			{
				ProjectionMatrix = FReversedZPerspectiveMatrix(
					FOV,
					FOV,
					XAxisMultiplier,
					YAxisMultiplier,
					GNearClippingPlane,
					GNearClippingPlane
				);
			}
			else
			{
				ProjectionMatrix = FPerspectiveMatrix(
					FOV,
					FOV,
					XAxisMultiplier,
					YAxisMultiplier,
					GNearClippingPlane,
					GNearClippingPlane
				);
			}
		}
	}
	
	FVector4 operator*(const FMatrix& a, const FVector4& b){
		FVector4 Result;
		

		Result.X=a.M[0][0] * b.X + a.M[0][1] * b.Y + a.M[0][2] * b.Z + a.M[0][3] * b.W;
		Result.Y = a.M[1][0] * b.X + a.M[1][1] * b.Y + a.M[1][2] * b.Z + a.M[1][3] * b.W;
		Result.Z = a.M[2][0] * b.X + a.M[2][1] * b.Y + a.M[2][2] * b.Z + a.M[2][3] * b.W;
		Result.W = a.M[3][0] * b.X + a.M[3][1] * b.Y + a.M[3][2] * b.Z + a.M[3][3] * b.W;
		return Result;
	}
};

bool UStrategyFunctionLibrary::ProjectWorldToScreen(USceneCaptureComponent2D* CaptureComponent, float DegreesHalfFOV,float wight, float height,const FVector& WorldPos,FVector4 & out_ScreenPos)
{

	FTransform Transform = CaptureComponent->GetComponentTransform();
	FVector ViewLocation = Transform.GetTranslation();

	FVector4 p(WorldPos, 1.f);

	FMatrix ViewMatrix = Transform.ToInverseMatrixWithScale();
	

	FViewMatrices vm;
	// swap axis st. x=z,y=x,z=y (unreal coord space) so that z is up
	ViewMatrix = ViewMatrix * FMatrix(
		FPlane(0, 0, 1, 0),
		FPlane(1, 0, 0, 0),
		FPlane(0, 1, 0, 0),
		FPlane(0, 0, 0, 1));

	//from unreal transform to common transform
	ViewMatrix = ViewMatrix.GetTransposed();


	const float RadiansHalfFOV = DegreesHalfFOV * (float)PI / 360;
	

	FMatrix ProjectionMatrix;

	FIntPoint CaptureSize(wight, height);
	
	BuildProjectionMatrix(CaptureSize, ECameraProjectionMode::Perspective, RadiansHalfFOV, 0/*not needed*/, ProjectionMatrix);

	p = ProjectionMatrix * (ViewMatrix*p);
	
	p.X /= p.W;
	p.Y /= p.W;
	p.Z /= p.W;
	p.Y *= -1.f;
	out_ScreenPos=p;
	

	
	
	return true;
}
