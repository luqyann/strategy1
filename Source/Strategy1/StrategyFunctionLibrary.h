// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "StrategyFunctionLibrary.generated.h"
class APlayerState;
/**
 * 
 */
UCLASS()
class STRATEGY1_API UStrategyFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	

public:
	UFUNCTION(BlueprintPure,Category="Strategy")
		static bool EqualPS(const TArray<APlayerState*>& a, const TArray<APlayerState*>& b);

	UFUNCTION(BlueprintPure, Category = "Strategy")
		static FMatrix GetMatrixFromTransform(const FTransform& t);

	UFUNCTION(BlueprintPure, Category = "Strategy")
		static FMatrix GetPerspectiveMatrix(float HalfFOVX, float HalfFOVY, float MultFOVX, float MultFOVY, float MinZ, float MaxZ);
	
	UFUNCTION(BlueprintPure, Category = "Strategy")
		static FMatrix MultiplyMatrix(const FMatrix& a, const FMatrix& b);

	
	UFUNCTION(BlueprintPure, Category = "Strategy")
		static bool ProjectWorldToScreen(class USceneCaptureComponent2D* CaptureComponent, float DegreesHalfFOV, float wight, float height, const FVector& WorldPos, FVector4 & out_ScreenPos);
	
};
